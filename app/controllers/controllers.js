// CONTROLLERS
myApp.controller('loginController', ['$scope', '$location', '$cookieStore', function($scope, $location, $cookieStore) {
    var vm = this;
    vm.login= login;

    function login(){
      vm.dataLoading = true;
        //ToDo save credentials in cookies and redirect to other page using $location.path('/');
        $cookieStore.put('email', vm.email);
        $location.path('/dashboard');

    }
}]);
//SIGN UP CONTROLLER
myApp.controller('registerController', ['$scope','$location','$http','$cookieStore', function($scope,$location,$http,$cookieStore) {
    var rg = this;
     rg.register = register;

    function register(){
        rg.dataLoading = true;
        $cookieStore.put('email', rg.email);
        $location.path('/dashboard');
    }

}]);

//DASHBOARD CONTROLLER

myApp.controller('dashboardController', ['$scope','$location','$http','$cookieStore',function($scope,$location,$http,$cookieStore) {

  // redirect here after login
    var ds = this;
    ds.userName = "salma";
    var userDataJson = 'app/json/userData.json';  //loading userdata fron json file
    var userData = userDataJson + '?v=' + (new Date().getTime()); // jumps cache
    $http.get(userData)
        .success(function (items) {
            console.log(items);
            ds.userName = items[0].userName;
            ds.imgPath = items[0].imgPath;
        })
    ds.email = $cookieStore.get('email');

    var productDataJson = 'app/json/products.json';
    var products = productDataJson + '?v=' + (new Date().getTime()); // jumps cache

    $http.get(products)
        .success(function (items) {  //loading products from json file
            console.log(items);
            ds.products = items;

        });

    //*************************************************************************************************
    //                                 function to confirm order
    //*************************************************************************************************
    $scope.showOrder = function(id){
        console.log(id);
        $location.path('/ordersummary/'+id);


    }



}]);

//ORDER SUMMARY CONTROLLER
myApp.controller('orderSummaryController', ['$scope','$location', '$cookieStore','$routeParams','$http', function($scope,$location, $cookieStore,$routeParams,$http) {
console.log($routeParams.productId);
    var os = this;
    var productDataJson = 'app/json/products.json';
    var products = productDataJson + '?v=' + (new Date().getTime()); // jumps cache

    $http.get(products)
        .success(function (items) {  //loading products from json file
            console.log(items);
            var productLength = items.length;
            for(var i=0;i<productLength;i++){
                if($routeParams.productId == items[i].productId){
                    os.productName = items[i].productName;
                    os.price = items[i].price;
                    os.vat = items[i].vat;
                    os.delivery_charge = items[i].delivery_charge;
                    os.imgPath = items[i].imgPath;
                    os.productId = items[i].productId;
                }
            }
           // if($routeParams.productId = items)

        });

}]);

//LOGOUT CONTROLLER

myApp.controller('logoutController', ['$scope','$location', '$cookieStore', function($scope,$location, $cookieStore) {

// logout here
    var lg = this;
    lg.logout = logout;
    function logout(){
        $cookieStore.remove('email');  //destroying the cookie containg email
        $location.path('/');
    }

}]);