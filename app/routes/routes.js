// ROUTES
myApp.config(function ($routeProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'app/views/home.htm'
        })
        .when('/dashboard', {
            templateUrl: 'app/views/dashboard.htm'
        })
        .when('/ordersummary/:productId', {
        templateUrl: 'app/views/ordersummary.html'
    })


});